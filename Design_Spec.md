# HTML CSS Workshop Design Spec

## Colours
* Font: #666666
* Links: #08C
* Headings: #FFFFFF and #000000

## Fonts
* Main font: "SF Pro Display"
* Fallback font: sans-serif
* Default font size: 16 pixels
* h2 weight: lighter
* h1: 70 pixels
* h2: 40 pixels

## Dimensions
* Content maximum width: 1350 pixels
* Banner height:  500 pixels
* Banner maximum width: 1350 pixels

## Image Assets
* sierra.jpg
* tv.jpg
* macbook.jpg
* whyipad.jpg

## Small screens (under 680 pixels of width)
* Banner height:  375 pixels
* Banner maximum width: 736 pixels
* h1: 40 pixels
* h2: 30 pixels
